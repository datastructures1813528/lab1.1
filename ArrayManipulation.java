public class ArrayManipulation {
    public static void main(String[] args) {
        
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"} ;
        double[] values = new double[4] ;

        
        for(int i=0; i<numbers.length; i++){
            System.out.print(numbers[i]+" ");
        }

        System.out.println();

        for(String c:names){
            System.out.print(c+" ") ;
        }

    }
}
